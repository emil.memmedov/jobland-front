import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Socket } from 'ngx-socket-io';
import * as io from 'socket.io-client';
//import {io} from 'socket.io-client';
import Echo from 'laravel-echo';

declare global {
  interface Window {
    io: any;
    Echo: any;
  }
}

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  scriptNode: any;
  readonly url: string = 'http://localhost:6001';
  socket: any;
  importScript(url: string) {
    this.scriptNode = document.createElement('script');
    this.scriptNode.src = url;
    this.scriptNode.type = 'text/javascript';
    this.scriptNode.async = true;
    document.getElementsByTagName('head')[0].appendChild(this.scriptNode);
  }
  echo: any;
  constructor() {
    window['io'] = io;
    this.echo = window['Echo'] = new Echo({
      client: io,
      broadcaster: 'socket.io',
      host: 'http://192.168.1.106:6001',
      //key: 'a9d5e079a38d682c257bb8481de3390b',
      //path: '/socket.io/socket.io.js',
      //auth: { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } }
    });
    this.importScript(this.url + '/socket.io/socket.io.js');
    setTimeout(() => {
      this.socket = window.io(this.url);
    },3000)
  }                   
  listen(eventName: string) {
    /*setTimeout(() => {
      console.log(this.socket);
    }, 3500);
    return new Observable((subscriber) => {
      setTimeout(() => {
        this.socket.on('.com.event', (ChannelName: any, data: any) => {
          console.log('here');
          subscriber.next(data);
        });
      }, 3500);
    });*/
    console.log(this.echo);
    this.echo.channel('client-comment').listen('CommentEvent', (data:any) => {
      console.log(data);
    })
  }
  emit(params: any) {
    /*this.socket.emit('CommentEvent', params);
    console.log(this.socket);*/
   }
}
