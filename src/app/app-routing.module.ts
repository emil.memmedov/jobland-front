import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SignPageComponent } from './pages/sign-page/sign-page.component';

const routes: Routes = [
  {
    path: 'sign', component: SignPageComponent
  },
  {
    path: 'home', component: HomePageComponent
  },
  {
    path: '', redirectTo: 'home', pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
