import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageServiceService {
  url = 'http://192.168.1.106/api/';
  token: any;
  categorisSub = new Subject();
  vacationsSub = new Subject();
  vacations: any = [];
  comments: any = [];
  commentsSub = new Subject();
  categories:{ id: Number, title: string }[] = [];



  constructor(private http: HttpClient) {
  }  
  getCategories() {
    this.http.get<{ id: Number, title: string }[]>(this.url + 'getcategories').subscribe(data => {
      this.categories = data;
      this.categorisSub.next(this.categories);
    })
  }
  getVacations(id:any) {
    this.http.get(this.url + 'getjobs/' + id).subscribe(data => {
      this.vacations = data;
      this.vacationsSub.next(this.vacations);
    })
  }
  showComments(index: any) {
    this.http.get(this.url + 'showcomments/' + index).subscribe(data => {
      this.comments = data;
      this.commentsSub.next(this.comments);
    })
  }
  logIn(email:any,password:any) {
    this.http.post(this.url + 'loginasclient', { email: email, password: password }).subscribe(data => {
      //@ts-ignore
      localStorage.setItem('token', data.token);
      //@ts-ignore
      this.token = data.token;
    });
  }
  sendComment(comment:any,id:any) {
    this.http.post(this.url + 'addcomment', { body: comment, vacation_id: id }, {
      headers: new HttpHeaders({
        'Authorization': 'Bearer'+localStorage.getItem('token')
      })
    }).subscribe(data => {
      console.log(data);
    });
  }






  
}
