import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { StorageServiceService } from 'src/app/storage-service.service';

@Component({
  selector: 'app-sign-page',
  templateUrl: './sign-page.component.html',
  styleUrls: ['./sign-page.component.css']
})
export class SignPageComponent implements OnInit {
  
  loginForm: FormGroup;
  constructor(private storageService: StorageServiceService) { 
    this.loginForm = new FormGroup({
      'email': new FormControl(null),
      'password':new FormControl(null)
    });
  }
  user = [true, true]; //user, sign in or sign up
  categories = ['IT', 'Medicine', 'Education', 'Design'];
  ngOnInit(): void {
  }

  ChangeToggle(e:any) {
    this.user[1] = !e.target.checked;
  }
  UserToggle(e:any) {
    this.user[0] = !e.target.checked;
  }
  logIn() {
    this.storageService.logIn('rrena.123456.rrena@gmail.com', 'rrena123456'); 
  }
}
