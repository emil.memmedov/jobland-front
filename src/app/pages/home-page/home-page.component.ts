import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SocketService } from 'src/app/socket.service';
import { StorageServiceService } from 'src/app/storage-service.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  auth = false;
  user = true;
  categories: { id: Number, title: string }[] = [];
  vacations: any = [];
  comments: any = [];
  openComment: boolean[] = [false, false];
  CommentForm: FormGroup;
  constructor(private storageService: StorageServiceService, private socket: SocketService) {
    this.CommentForm = new FormGroup({
      'comment': new FormControl(null)
    })
    this.storageService.getCategories();
    this.storageService.categorisSub.subscribe(data => {
      //@ts-ignore
      this.categories = this.storageService.categories.categories;
    })
    this.storageService.vacationsSub.subscribe(data => {
      this.vacations = this.storageService.vacations['vacations'];
      for (let i = 0; i < this.vacations.length; i++){
        this.openComment.push(false);
      }
    })
    this.storageService.commentsSub.subscribe(data => {
      this.comments = this.storageService.comments['comments'];
    })
  }
  ngOnInit(): void {
    this.socket.listen('comment');/*.subscribe((resp: any) => {
      console.log('socket-data: ', resp);
    });*/
  }

  ShowComments(index: any,i:any) {
    this.openComment[i] = !this.openComment[i];
    this.storageService.showComments(index);
  }
  getVacations(id:any) {
    this.storageService.getVacations(id);
  }
  sendComment(id:any) {
    this.storageService.sendComment(this.CommentForm.value.comment, id);
    let params = {
      'comment': this.CommentForm.value.comment,
      'id':id
    }
    this.socket.emit(params);
  }
}
